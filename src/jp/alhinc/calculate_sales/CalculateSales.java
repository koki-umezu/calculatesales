package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_CONSECUTIVE_NUMBER = "売り上げファイル名が連番にはなっていません";
	private static final String SALEAMOUNT_OVERFLOW = "合計金額が10桁を超えました";
	private static final String MAP_DONT_HAVE_CODE = "の支店コードが不正です";
	private static final String MAP_DONT_HAVE_COMMODITY_CODE = "の商品コードが不正です";
	private static final String LIST_SIZE_OVERFLOW = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();

		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();

		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// エラー処理：コマンドライン引数が渡されているかの判定
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 処理:支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales)) {
			return;
		}

		// 処理:商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales)) {
			return;
		}

		// 処理:集計処理
		// 定義:rcdファイルの判別と格納するためのオブジェクトの定義
		File[] files = new File(args[0]).listFiles();
		ArrayList<File> rcdFiles = new ArrayList<>();

		// 処理:rcdファイルの判別と格納処理
		for(int i = 0; i < files.length ; i++) {
			if(files[i].isFile() && files[i].getName().matches("^\\d{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}

		// エラー処理：rcdファイルが連番かの判定
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i+1).getName().substring(0, 8));
			if((latter - former) != 1) {
				System.out.println(FILE_NOT_CONSECUTIVE_NUMBER);
				return;
			}
		}

		// 処理:rcdファイルの読み込みと売上金額の格納処理
		for(int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;

			// 処理:rcdファイルが読み込めるかの判別処理
			try {
				// 定義:ファイル読み込みのための変数の定義
				String tmpFileName = rcdFiles.get(i).getName();
				File file = new File(args[0], tmpFileName);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				String line;

				// 定義:読み込んだrcdファイルの値を格納する配列の定義
				ArrayList<String> rcdItems = new ArrayList<String>();

				// 処理:読み込んだrcdファイルの値の格納処理
				while((line = br.readLine()) != null) {
					rcdItems.add(line);
				}

				// 定義:読み込んだ売り上げ情報を一時格納するための定義
				String storeCode = rcdItems.get(0);
				String itemCode = rcdItems.get(1);
				String tmpSaleAmount =rcdItems.get(2);

				// エラー処理:売り上げファイルのフォーマットの判定
				if(rcdItems.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + LIST_SIZE_OVERFLOW);
					return;
				}
				// エラー処理:売上金額のフォーマットの判定
				if(!tmpSaleAmount.matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				// エラー処理:売り上げファイルの支店コードの判定
				if(!branchSales.containsKey(storeCode)) {
					System.out.println(tmpFileName + MAP_DONT_HAVE_CODE);
					return;
				}
				// エラー処理:売り上げファイルの商品コードの判定
				if(!commodityNames.containsKey(itemCode)) {
					System.out.println(tmpFileName + MAP_DONT_HAVE_COMMODITY_CODE);
					return;
				}

				// 処理:売り上げファイルから売上金額の抽出と一時配列への格納処理
				long fileSale = Long.parseLong(tmpSaleAmount);
				Map<String, Long> saleAmount = new HashMap<>();
				saleAmount.put(storeCode, branchSales.get(storeCode) + fileSale);
				saleAmount.put(itemCode, commoditySales.get(itemCode) + fileSale);

				// エラー処理:合計の売り上げ金額が10桁以上かの判定
				if(saleAmount.get(storeCode) >= 10000000000L || saleAmount.get(itemCode) >= 10000000000L) {
					System.out.println(SALEAMOUNT_OVERFLOW);
					return;
				}

				// 処理:マップへ売上金額を格納する処理
				branchSales.put(storeCode, saleAmount.get(storeCode));
				commoditySales.put(itemCode, saleAmount.get(itemCode));

			// 処理:rcdファイルが読み込めない場合の処理
			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;

			// 処理:rcdファイルを閉じる処理
			}finally {
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 処理:支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 処理:商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 処理1:支店または商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店または商品コードと支店または商品名を保持するMap
	 * @param 支店または商品コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String>
		targetNames, Map<String, Long> targetSales) {

		BufferedReader br = null;
		String regex = null;
		String type = null;

		// 処理:ファイルの名前からエラーメッセージと正規表現を割り当てる処理
		switch (fileName){
		case "branch.lst":
			regex = "^\\d{3}$";
			type = "支店";
			break;
		case "commodity.lst":
			regex = "^[A-Za-z0-9]{8}$";
			type = "商品";
			break;
		}

		// 処理:支店定義ファイルが読み込めるかの判別処理
		try {
			// 定義:ファイル読み込みのための変数の定義
			File file = new File(path, fileName);

			// エラー処理:支店または商品定義ファイルが存在するかの判定
			if(!file.exists()) {
				System.out.println(type + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			// 処理:読み込んだ支店または商品定義ファイルの値の格納処理
			while((line = br.readLine()) != null) {
				//処理:書き込んである支店または商品情報の分割と格納処理
				String[] items = line.split(",");

				// エラー処理:支店または商品定義ファイル内部のフォーマットの判定
				if(items.length != 2 || !items[0].matches(regex)) {
					System.out.println(type + FILE_INVALID_FORMAT);
					return false;
				}

				// 処理:支店または商品情報をマップに格納する処理
				targetNames.put(items[0], items[1]);
				targetSales.put(items[0], 0L);

			}
		// 処理:支店または商品定義ファイルが読み込めない場合の処理
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		// 処理:支店または商品定義ファイルを閉じる処理
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別または商品別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String>
		targetNames, Map<String, Long> targetSales) {

		BufferedWriter bw = null;

		// 処理:支店別または商品別集計ファイルが読み込めるかの判別処理
		try {
			// 定義:定義:ファイル読み込みのための変数の定義
			File file =new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			// 処理:支店別または商品別集計ファイルへの書き込み処理
			for(String key : targetNames.keySet()) {
				bw.write(key + "," + targetNames.get(key) + "," + targetSales.get(key));
				bw.newLine();
			}

		// 処理:支店別または商品別集計ファイルが読み込めない場合の処理
		} catch(IOException e){
			System.out.println(UNKNOWN_ERROR);
			return false;

		// 処理:支店別または商品別集計ファイルを閉じる処理
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
